﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Domain
{
    public class BlogsContextInitializer : DropCreateDatabaseIfModelChanges<BlogsContext>//DropCreateDatabaseIfModelChanges<BlogsContext> //DropCreateDatabaseAlways<BlogsContext>
    {
        /// <summary>
        /// Add some seed data when database is first created
        /// </summary>
        /// <param name="context">Database context</param>
        protected override void Seed(BlogsContext context)
        {
            base.Seed(context);

            #region categories

            var categories = new List<Category>()             
            { 
                new Category () { Name = "Music" },
                new Category () { Name = "Technology" },
                new Category () { Name = "Social media" }
            };

            categories.ForEach(p => context.Categories.Add(p));

            #endregion


            #region tags

            var tags = new List<Tag>()             
            { 
                new Tag () { Name = "Jazz" },
                new Tag () { Name = "iOS" },
                new Tag () { Name = "Android" }
            };

            tags.ForEach(p => context.Tags.Add(p));

            #endregion


            //save to DB
            context.SaveChanges();


            #region blogs
            
            var blogs = new List<Blog>()             
            { 
                new Blog() { Title = "Blog about Jazz", Description="Cool blog about Jazz", Body="Lorem ipsum about Jazz", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, Category = categories[0] },
                new Blog() { Title = "Blog about mobile phones", Description="Cool blog about mobile phones", Body="Lorem ipsum about mobile", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, Category = categories[1] },
                new Blog() { Title = "Blog about tablets", Description="Cool blog about tablets", Body="Lorem ipsum about tablets", CreatedDate = DateTime.Now, ModifiedDate = DateTime.Now, Category = categories[1] }
                
            };



            //add talk backs
            TalkbackReply reply = new TalkbackReply();
            reply.BlogId = blogs[0].Id;
            reply.Title = "This is a Jazz blog talkback";
            reply.Comment = "Amazing blog!";
            reply.SubmitterName = "John Doe";
            reply.SubmittedDate = DateTime.Now;
            reply.IsApproved = true;
            reply.IsDeleted = false;

            blogs[0].TalkBacks = new List<TalkbackReply> { reply };
            //add tags
            blogs[0].Tags = new List<Tag> { tags[0] };
            //add talkbacks
            blogs.ForEach(p => context.Blogs.Add(p));


            reply = new TalkbackReply();
            reply.BlogId = blogs[0].Id;
            reply.Title = "This another a Jazz blog talkback";
            reply.Comment = "Super amazing blog!";
            reply.SubmitterName = "Sam Smith";
            reply.SubmittedDate = DateTime.Now;
            reply.IsApproved = true;
            reply.IsDeleted = false;

            blogs[0].TalkBacks.Add(reply);
            //add talkbacks
            blogs.ForEach(p => context.Blogs.Add(p));


            reply = new TalkbackReply();
            reply.BlogId = blogs[1].Id;
            reply.Title = "This mobile phone blog talkback";
            reply.Comment = "Cool info on mobile phones!";
            reply.SubmitterName = "Mary Smith";
            reply.SubmittedDate = DateTime.Now;
            reply.IsApproved = true;
            reply.IsDeleted = false;

            blogs[1].TalkBacks = new List<TalkbackReply> { reply };
            //add talkbacks
            blogs.ForEach(p => context.Blogs.Add(p));
            //add tags
            blogs[1].Tags = new List<Tag> { tags[1], tags[2] };


            #endregion


            //save to DB
            context.SaveChanges();


            //add log4net table
            using (System.IO.StreamReader sr = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Readme/log4net.sql")))
            {
                string sql = sr.ReadToEnd();

                try
                {
                    context.Database.ExecuteSqlCommand(sql);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }


            using (System.IO.StreamReader sr = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Readme/Elmah.sql")))
            {
                string sql = sr.ReadToEnd();

                string[] commands = sql.Split(new string[] { "GO" }, StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    foreach (var command in commands)
                    {
                        context.Database.ExecuteSqlCommand(command);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }


        }

    }
}