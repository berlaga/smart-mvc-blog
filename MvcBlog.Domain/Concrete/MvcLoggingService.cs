﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

namespace MvcBlog.Domain
{
    public class MvcLoggingService : ILoggingService
    {
        private static readonly ILog _logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MvcLoggingService()
        {

        }

        public void Info(string message)
        {
            _logger.Info(message);
        }
        public void InfoFormat(string message, Exception ex)
        {
            _logger.InfoFormat(message, ex);
        }


        public void Error(string message)
        {
            _logger.Error(message);
        }
        public void ErrorFormat(string message, Exception ex)
        {
            _logger.ErrorFormat(message, ex);
        }


        public void Debug(string message)
        {
            _logger.Debug(message);
        }
        public void DebugFormat(string message, Exception ex)
        {
            _logger.DebugFormat(message);
        }


        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }
        public void FatalFormat(string message, Exception ex)
        {
            _logger.FatalFormat(message);
        }


        public void Warn(string message)
        {
            _logger.Warn(message);
        }
        public void WarnFormat(string message, Exception ex)
        {
            _logger.WarnFormat(message);
        }

    }
}
