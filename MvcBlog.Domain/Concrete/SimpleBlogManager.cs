﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Domain
{
    public class SimpleBlogManager : IBlogManager, IDisposable
    {
        private BlogsContext _blogContext = new BlogsContext();

        #region IBlogManager Members

        public List<Blog> GetAllBlogs()
        {
            //enforce Lazy Loading by using Include
            List<Blog> list = _blogContext.Blogs.Include("TalkBacks").Include("Tags").ToList();
            return list;
        }

        public List<Blog> GetAllBlogs(string searchToken)
        {
            if (string.IsNullOrEmpty(searchToken))
                return GetAllBlogs();

            //enforce Lazy Loading by using Include
            List<Blog> list = _blogContext.Blogs.Include("TalkBacks").Include("Tags").Where(p => (p.Title.Contains(searchToken))
                || (p.Description.Contains(searchToken)) || (p.Body.Contains(searchToken))).ToList();
            return list;
        }



        public Blog GetBlog(int id)
        {
            Blog blog = _blogContext.Blogs.Include("TalkBacks").Include("Tags").First(p=>p.Id == id);
            if (blog == null)
            {
                throw new ApplicationException(string.Format("Blog ID={0} not found", id));
            }

            return blog;
        }

        /// <summary>
        /// Get Blog and talk backs that match criteria
        /// </summary>
        /// <param name="id">Blog ID</param>
        /// <param name="includeDeleted">Deleted talkbacks</param>
        /// <param name="approvedOnly">Approved talkbacks</param>
        /// <returns></returns>
        public Blog GetBlog(int id, bool includeDeleted, bool approvedOnly)
        {
            //create projection
            var query = _blogContext.Blogs.Where(p=>p.Id == id).Select(p => new
                {
                    Id = p.Id,
                    CategoryId = p.CategoryId,
                    Title = p.Title,
                    Body = p.Body,
                    Description = p.Description,
                    CreatedDate = p.CreatedDate,
                    ModifiedDate = p.ModifiedDate,
                    Tags = p.Tags,
                    TalkBacks = p.TalkBacks.Where(c => c.IsApproved == approvedOnly && c.IsDeleted == includeDeleted)

                }
                );

            Blog blog = query.ToList().Select(p => new Blog {
                Id = p.Id,
                CategoryId = p.CategoryId,
                Title = p.Title,
                Body = p.Body,
                Description = p.Description,
                CreatedDate = p.CreatedDate,
                ModifiedDate = p.ModifiedDate,
                Tags = p.Tags,
                TalkBacks = p.TalkBacks.ToList()
            
            
            }
            ).FirstOrDefault();


            if (blog == null)
            {
                throw new ApplicationException(string.Format("Blog ID={0} not found", id));
            }

            return blog;
        }


        /// <summary>
        /// Creates new Blog and associated objects 
        /// </summary>
        /// <param name="newBlogEntry"></param>
        /// <returns></returns>
        public int CreateBlog(Blog newBlogEntry)
        {
            try
            {
                List<Tag> newTagList = new List<Tag>();

                if((newBlogEntry.Tags != null) && (newBlogEntry.Tags.Count > 0))
                {
                    foreach (Tag t in newBlogEntry.Tags)
                    {
                        //check if this tag already exist
                        List<Tag> existingTags = _blogContext.Tags.Where(p => p.Name.Equals(t.Name, StringComparison.InvariantCultureIgnoreCase)).ToList();

                        if (existingTags.Count > 0) // if Tag exists add it to Tag collection
                            newTagList.AddRange(existingTags);
                        else
                            newTagList.Add(t); //otherwise add as new Tag
                    }
                }

                newBlogEntry.Tags = newTagList;

                _blogContext.Blogs.Add(newBlogEntry);
                _blogContext.SaveChanges();

                return newBlogEntry.Id;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to create new blog", ex);
            }

        }

        public void UpdateBlog(Blog updatedBlog)
        {
            //load original from DB
            Blog originalBlog = this.GetBlog(updatedBlog.Id);
            List<Tag> originalTagList = originalBlog.Tags;

            List<Tag> newList = updatedBlog.Tags.Except(originalTagList, new MvcBlog.Domain.Concrete.TagComparer()).ToList();
            //List<Tag> existList = originalList.Intersect(updatedBlog.Tags, new MvcBlog.Domain.Concrete.TagComparer()).ToList();
            List<Tag> deletedList = originalTagList.Except(updatedBlog.Tags, new MvcBlog.Domain.Concrete.TagComparer()).ToList();

            originalBlog.Title = updatedBlog.Title;
            originalBlog.Body = updatedBlog.Body;
            originalBlog.CategoryId = updatedBlog.CategoryId;
            originalBlog.Description = updatedBlog.Description;
            originalBlog.ModifiedDate = updatedBlog.ModifiedDate;


            try
            {
                //process removed Tags
                foreach (Tag t in deletedList)
                {
                    _blogContext.Tags.Attach(t);
                    _blogContext.ObjectContext().ObjectStateManager.ChangeRelationshipState(originalBlog, t, p => p.Tags, System.Data.EntityState.Deleted);
                }

                //process new and added Tags 
                foreach (Tag t in newList)
                {
                    //check if this tag already exist
                    Tag existingTag = _blogContext.Tags.FirstOrDefault(p => p.Name.Equals(t.Name, StringComparison.InvariantCultureIgnoreCase));

                    if (existingTag != null) // This tag exists already, just map it
                    {
                        _blogContext.Tags.Attach(existingTag);
                        _blogContext.ObjectContext().ObjectStateManager.ChangeRelationshipState(originalBlog, existingTag, p => p.Tags, System.Data.EntityState.Added);
                    }
                    else
                        originalBlog.Tags.Add(t); //otherwise add as new Tag
                }


                _blogContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Failed to save changes to blog ID={0}", updatedBlog.Id), ex);
            }
        }

        public void DeleteBlog(int blogId)
        {
            Blog blog = _blogContext.Blogs.Find(blogId);
            if (blog == null)
            {
                throw new ApplicationException(string.Format("Failed to find blog ID={0}", blogId));
            }

            //this will cascade delete all TalkbackReplies for the blog as well
            _blogContext.Blogs.Remove(blog);

            try
            {
                _blogContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new ApplicationException(string.Format("Failed to delete blog ID={0}", blogId), ex);
            }

        }


        public List<Blog> GetBlogsByTagId(int id)
        {
            List<Blog> list = _blogContext.Tags.
                Include("Blogs").
                    Where(p => p.Id == id).
                    SelectMany(x=>x.Blogs).
                    Distinct().
                    ToList();



            return list;
        }

        #endregion

        public void Dispose()
        {
            _blogContext.Dispose();
        }


    }
}