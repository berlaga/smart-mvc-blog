﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;

namespace MvcBlog.Domain
{
    public class SimpleDBHelper : IDBHelper 
    {
        private BlogsContext _blogContext = new BlogsContext();

        /// <summary>
        /// Get sorted list of Categories
        /// </summary>
        /// <returns></returns>
        public List<Entities.Category> GetCategories()
        {
            return _blogContext.Categories.OrderBy(p=>p.Name).ToList();
        }

        /// <summary>
        /// Get category by Id
        /// </summary>
        /// <returns></returns>
        public Entities.Category GetCategory(int Id)
        {
            Entities.Category category = _blogContext.Categories.Find(new object [] {Id});

            return category;
        }


        public List<Entities.Tag> GetTags()
        {
            return _blogContext.Tags.ToList();
        }


        public Entities.Tag GetTag(int id)
        {
            Entities.Tag tag = _blogContext.Tags.Find(new object[] { id });
            
            return tag;
        }

        public List<Entities.Tag> GetTagsByName(string name)
        {
            List<Entities.Tag> tags = _blogContext.Tags.Include("Blogs").Where(p => p.Name.ToLower().Equals(name.ToLower(), StringComparison.CurrentCultureIgnoreCase)).ToList();

            return tags;
        }


        public void UpdateTag(Entities.Tag newTag)
        {
            List<Entities.Tag> list = this.GetTagsByName(newTag.Name);

            if (list.Where(p => p.Name.ToLower().Equals(newTag.Name, StringComparison.InvariantCultureIgnoreCase) && p.Id != newTag.Id).Count() > 0)
                throw new ApplicationException("This value already exist");

            Entities.Tag tagOriginal = this.GetTag(newTag.Id);

            if (tagOriginal != null)
            {
                tagOriginal.Name = newTag.Name;

                _blogContext.SaveChanges();
            }
        }


        public void DeleteTag(int id)
        {
            Entities.Tag tag = this.GetTag(id);

            if (tag != null)
            {
                _blogContext.Tags.Remove(tag);

                try
                {
                    _blogContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw new ApplicationException(string.Format("Failed to delete tag ID={0}", id), ex);
                }

            }
            else
                throw new ApplicationException("Tag not found.");

        }


        public List<Entities.Tag> GetTagsForBlog(int blogId)
        {
            List<Entities.Tag> tags = _blogContext.Blogs.First(p => p.Id == blogId).Tags;

            return tags;
        }


        public Entities.Tag GetAnyExistingTag()
        {
            return _blogContext.Tags.FirstOrDefault();
        }


        public int CreateTag(Entities.Tag newTag)
        {
            try
            {
                Entities.Tag t = _blogContext.Tags.FirstOrDefault(p => p.Name == newTag.Name);

                if (t != null)
                    return t.Id;

                _blogContext.Tags.Add(newTag);
                _blogContext.SaveChanges();

                t = _blogContext.Tags.FirstOrDefault(p => p.Name == newTag.Name);

                return t.Id;

            }
            catch (Exception ex)
            {
                    throw new ApplicationException("Failed to create new tag", ex);
            }

        }


    }
}
