﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Domain.Concrete
{
    public class TagComparer : IEqualityComparer<Tag>
    {
        public bool Equals(Tag x, Tag y)
        {
            //Check whether the compared object is null. 
            if (Object.ReferenceEquals(x, null)) return false;

            //Check whether the compared object is null. 
            if (Object.ReferenceEquals(y, null)) return false;

            //Check whether the compared object references the same data. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether the products' properties are equal. 
            return x.Name.Equals(y.Name, StringComparison.CurrentCultureIgnoreCase);
        }

        public int GetHashCode(Tag obj)
        {
            //Get hash code for the Name field if it is not null.
            int hashProductName = obj.Name == null ? 0 : obj.Name.GetHashCode();

            //Calculate the hash code for the product.
            return hashProductName;
        }
    }

    

}
