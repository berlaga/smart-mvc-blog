﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Domain
{
    public class SimpleTalkbackManager : ITalkbackManager
    {
        private BlogsContext _blogContext = new BlogsContext();


        #region ITalkbackManager Members

        public List<TalkbackReply> GetBlogTalkbacks(int blogId)
        {
            return _blogContext.Blogs.Include("TalkBacks").Single(p => p.Id == blogId).TalkBacks.ToList();
        }

        public int CreateTalkback(TalkbackReply newReply)
        {
            TalkbackReply reply = _blogContext.Talkbacks.Add(newReply);
            _blogContext.SaveChanges();

            if (reply == null)
                throw new ApplicationException("Failed to add new reply");

            return reply.Id;
            
        }



        public void UpdateTalkback(TalkbackReply updateReply)
        {
            _blogContext.Talkbacks.Attach(updateReply);
            _blogContext.Entry(updateReply).State = System.Data.EntityState.Modified;
            _blogContext.SaveChanges();

        }


        public void DeleteTalkback(int replyId)
        {
            _blogContext.Talkbacks.Remove(_blogContext.Talkbacks.First(p => p.Id == replyId));
            _blogContext.SaveChanges();
        }

        #endregion

    }
}