﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WebMatrix.WebData;
using System.Web.Security;

namespace MvcBlog.Domain
{
    public class SimpleUserManager : IUserManager
    {
        private BlogsContext _blogContext = new BlogsContext();

        public Entities.UserProfile GetUserProfile(string userName)
        {
            Entities.UserProfile profile = _blogContext.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userName.ToLower());

            return profile;
        }

        public Entities.UserProfile GetUserProfile(int userId)
        {
            throw new NotImplementedException();
        }


        public int AddUserProfile(Entities.UserProfile userProfileNew)
        {
            // Insert name into the profile table
            _blogContext.UserProfiles.Add(userProfileNew);
            _blogContext.SaveChanges();

            Entities.UserProfile profile = _blogContext.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == userProfileNew.UserName.ToLower());

            return profile.UserId;
        }


        public IEnumerable<Entities.UserProfile> GetUsers(bool excludeAdmins)
        {
            List<Entities.UserProfile> listUsers = new List<Entities.UserProfile>();

            var users = _blogContext.UserProfiles;

            if (excludeAdmins)
            {
                var roles = (SimpleRoleProvider)Roles.Provider;

                foreach (var user in users)
                {
                    if (!roles.IsUserInRole(user.UserName, "Admin"))
                    {
                        listUsers.Add(user);
                    }
                }

                return listUsers;
            }

            return users;
        }


        public IEnumerable<Entities.UserProfile> GetAdminUsers()
        {
            List<Entities.UserProfile> listUsers = new List<Entities.UserProfile>();


            var roles = (SimpleRoleProvider)Roles.Provider;

            var users = _blogContext.UserProfiles.Where(p=>roles.IsUserInRole(p.UserName, "Admin") == true);


            foreach (var user in users)
            {
                if (roles.IsUserInRole(user.UserName, "Admin"))
                {
                    listUsers.Add(user);
                }
            }

            return listUsers;

        }

    }
}
