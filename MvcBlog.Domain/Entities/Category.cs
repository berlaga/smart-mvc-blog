﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace MvcBlog.Domain.Entities
{
    [Table("Category")]
    public class Category
    {
        [ScaffoldColumn(false)] 
        public int Id { get; set; }

        [Required(ErrorMessage = "Category name is mandatory field")]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}
