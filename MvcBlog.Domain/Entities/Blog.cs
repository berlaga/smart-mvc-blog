﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcBlog.Domain.Entities
{
    /// <summary>
    /// Encapsulates blog entry
    /// </summary>
    /// 
    [Table("Blog")]
    public class Blog
    {
        [ScaffoldColumn(false)] 
        public int Id { get; set; }

        public Category Category { get; set; }
        //[RegularExpression("/^[1-9][0-9]*$/", ErrorMessage = "Category is mandatory field")]
        //[Required(ErrorMessage = "Category is mandatory field")]
        public int CategoryId { get; set; }
        
        [Required(ErrorMessage="Title is mandatory field")]
        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Body is mandatory field")]
        public string Body { get; set; }
        
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ModifiedDate { get; set; }

        
        public List<TalkbackReply> TalkBacks { get; set; }
        public List<Tag> Tags { get; set; }

    }


}