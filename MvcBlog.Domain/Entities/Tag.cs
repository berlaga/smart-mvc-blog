﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace MvcBlog.Domain.Entities
{
    [Table("Tag")]
    public class Tag
    {
        [ScaffoldColumn(false)] 
        public int Id { get; set; }

        [Required(ErrorMessage = "Tag name is mandatory field")]
        [MaxLength(50)]
        public string Name { get; set; }

        public List<Blog> Blogs { get; set; }

    }
}
