﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 

namespace MvcBlog.Domain.Entities
{
    /// <summary>
    /// Encapsulates blog talk back
    /// </summary>
    /// 
    [Table("TalkbackReply")]
    public class TalkbackReply
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }

        [ScaffoldColumn(false)]
        public int UserId { get; set; }

        //[Required] there is a bug in MVC4
        [Range(0,1)]
        public bool IsApproved { get; set; }

        [Range(0, 1)]
        public bool IsDeleted { get; set; }

        [Required]
        public string Comment { get; set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(100)]
        public string SubmitterEmail { get; set; }

        [MaxLength(50)]
        public string SubmitterName { get; set; }

        //[Required] there is a bug in MVC4
        ////http://aspnetwebstack.codeplex.com/workitem/270
        ////http://stackoverflow.com/questions/12305784/dataannotation-for-required-property
        [DataType(DataType.DateTime)]
        public DateTime SubmittedDate { get; set; }
    }
}
