﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
﻿using System.Data.Entity;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Domain
{
    public class BlogsContext : DbContext
    {
        public BlogsContext()
            : base("name=DefaultConnection") //connection string name from Web.config
        {
            #if DEBUG 
            Database.SetInitializer(new BlogsContextInitializer());
            #endif
        }

        public ObjectContext ObjectContext()
        {
            return (this as IObjectContextAdapter).ObjectContext;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //configure model with fluent API if needed
        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<TalkbackReply> Talkbacks { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

    }
}