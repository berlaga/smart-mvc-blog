﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Domain
{
    public interface IBlogManager
    {
        List<Blog> GetAllBlogs();
        List<Blog> GetAllBlogs(string searchToken);
        List<Blog> GetBlogsByTagId(int id);
        Blog GetBlog(int id);
        /// <summary>
        /// Return blog with TalkBack replies filtered by conditions
        /// </summary>
        /// <param name="id">Blog ID</param>
        /// <param name="includeDeleted">Include deleted replies</param>
        /// <param name="approvedOnly">Include approved replies</param>
        /// <returns></returns>
        Blog GetBlog(int id, bool includeDeleted, bool approvedOnly);
        int CreateBlog(Blog newBlogEntry);
        void UpdateBlog(Blog blogEntry);
        void DeleteBlog(int blogId);
    }


    public interface IUserManager
    {
        UserProfile GetUserProfile(string userName);
        UserProfile GetUserProfile(int userId);
        int AddUserProfile(UserProfile userProfileNew);

    }


    public interface ITalkbackManager
    {
        List<TalkbackReply> GetBlogTalkbacks(int blogId);
        int CreateTalkback(TalkbackReply newReply);
        void UpdateTalkback(TalkbackReply newReply);
        void DeleteTalkback(int replyId);

    }

    public interface IDBHelper
    {
        List<Category> GetCategories();
        Category GetCategory(int Id);
        List<Tag> GetTags();
        Tag GetTag(int id);
        void UpdateTag(Tag Tag);
        void DeleteTag(int id);
        int CreateTag(Tag newTag);
        List<Tag> GetTagsForBlog(int blogId);
        Tag GetAnyExistingTag();

    }

    public interface ILoggingService
    {
        void Info(string message);
        void Error(string message);
        void Debug(string message);
    }
}