﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MvcBlog.Domain;
using MvcBlog.Domain.Entities;


namespace MvcBlog.Tests
{
    [TestClass]
    public class SimpleBlogManagerTest
    {
        private List<Blog> _blogList = new List<Blog>();
        private List<Tag> _tagList = new List<Tag>();

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //

        #endregion


        //Use TestCleanup to run code after each test has run
        [TestCleanup]
        public void SimpleBlogManagerTestCleanup()
        {
            if (_blogList.Count > 0)
            {
                SimpleBlogManager manager = new SimpleBlogManager();
                SimpleDBHelper helper = new SimpleDBHelper();

                //delete all tags
                _blogList.ForEach(p => p.Tags.ForEach(a => helper.DeleteTag(a.Id)));

                //delete blog
                _blogList.ForEach(p => manager.DeleteBlog(p.Id));

                _tagList.ForEach(p => helper.DeleteTag(p.Id));
            }

            _blogList.Clear();
        }
        

        [TestMethod]
        public void CreateBlogTest()
        {
            SimpleBlogManager manager = new SimpleBlogManager();

            DateTime dt = DateTime.Now;

            Blog blog = new Blog();
            blog.CategoryId = 1;
            blog.Body = "Test blog, created on " + dt.ToString("g");
            blog.Title = "Test Title, created on " + dt.ToString("g");
            blog.Description = "Test description " + dt.ToString("g");
            blog.CreatedDate = dt;
            blog.ModifiedDate = dt.AddSeconds(2);

            List<Tag> tags = new List<Tag>();
            
            Tag t1 = new Tag();
            t1.Name = "tag " + dt.ToString("g");

            Tag t2 = new Tag();
            t2.Name = "tag1 " + dt.ToString("g");

            tags.Add(t1);
            tags.Add(t2);

            blog.Tags = tags;

            int res = manager.CreateBlog(blog);

            Assert.IsTrue(res > 0);

            Blog newBlog = manager.GetBlog(res);

            Assert.IsTrue(newBlog != null);
            Assert.IsTrue(newBlog.Tags.Count > 0);

            //for after-test cleanup
            _blogList.Add(newBlog);
        }

        [TestMethod]
        public void EditBlogTest()
        {
            #region create blog

            SimpleBlogManager manager = new SimpleBlogManager();

            DateTime dt = DateTime.Now;

            Blog blog = new Blog();
            blog.CategoryId = 1;
            blog.Body = "Test blog, created on " + dt.ToString("g");
            blog.Title = "Test Title, created on " + dt.ToString("g");
            blog.Description = "Test description " + dt.ToString("g");
            blog.CreatedDate = dt;
            blog.ModifiedDate = dt;

            List<Tag> tags = new List<Tag>();

            Tag t1 = new Tag();
            t1.Name = "tag1 " + dt.ToString("g");

            Tag t2 = new Tag();
            t2.Name = "tag2 " + dt.ToString("g");

            tags.Add(t1);
            tags.Add(t2);

            blog.Tags = tags;

            int res = manager.CreateBlog(blog);

            Assert.IsTrue(res > 0);

            Blog existingBlog = manager.GetBlog(res);

            Assert.IsTrue(existingBlog != null);
            Assert.IsTrue(existingBlog.Tags.Count > 0);

            _blogList.Add(existingBlog);

            #endregion

            existingBlog.ModifiedDate = DateTime.Now.AddSeconds(5);
            blog.CategoryId = 2;
            blog.Body = "Updated blog, created on " + existingBlog.ModifiedDate.ToString("g");
            blog.Title = "Updated Title, created on " + existingBlog.ModifiedDate.ToString("g");
            blog.Description = "Updated description " + existingBlog.ModifiedDate.ToString("g");

            //remove 1 tag
            Tag tag1 = blog.Tags[0];
            blog.Tags.Remove(tag1);

            //helper code for after-test cleanup
            _tagList.Add(tag1);

            Tag tagNew = new Tag();
            tagNew.Name = "new tag " + existingBlog.ModifiedDate.ToString("g");

            blog.Tags.Add(tagNew);

            SimpleDBHelper helper = new SimpleDBHelper();
            Tag existingTag = new Tag();
            existingTag.Name = "Pizza";

            blog.Tags.Add(existingTag);

            manager.UpdateBlog(existingBlog);

            existingBlog = manager.GetBlog(res);

            Assert.IsTrue(existingBlog.Body.Contains("Updated"));
            Assert.IsTrue(existingBlog.Title.Contains("Updated"));
            Assert.IsTrue(existingBlog.Description.Contains("Updated"));
        }

        [TestMethod]
        public void DeleteBlogTest()
        {
            SimpleBlogManager manager = new SimpleBlogManager();

            DateTime dt = DateTime.Now;

            Blog blog = new Blog();
            blog.CategoryId = 1;
            blog.Body = "Test blog, created on " + dt.ToString("g");
            blog.Title = "Test Title, created on " + dt.ToString("g");
            blog.Description = "Test description " + dt.ToString("g");
            blog.CreatedDate = dt;
            blog.ModifiedDate = dt.AddSeconds(2);


            TalkbackReply reply = new TalkbackReply();
            reply.IsApproved = true;
            reply.IsDeleted = false;
            reply.Title = "Test reply";
            reply.Comment = "Test reply comment";
            reply.SubmittedDate = DateTime.Now;
            reply.SubmitterName = "Some Guy";
            reply.SubmitterEmail = "test@test.com";
            reply.BlogId = blog.Id;

            blog.TalkBacks = new List<TalkbackReply> { reply };

            int res = manager.CreateBlog(blog);

            Assert.IsTrue(res > 0);

            Blog newBlog = manager.GetBlog(res);

            Assert.IsTrue(newBlog != null);

            manager.DeleteBlog(newBlog.Id);

            Blog b = null;
            try
            {
                b = manager.GetBlog(newBlog.Id);
            }
            catch (Exception)
            {
            }

            SimpleTalkbackManager talkBackManager = new SimpleTalkbackManager();

            List<TalkbackReply> t = null;
            try
            {
                t = talkBackManager.GetBlogTalkbacks(newBlog.Id);
            }
            catch (Exception)
            {
            }

            Assert.AreEqual(b, null);
            Assert.AreEqual(t, null);

        }


    }
}
