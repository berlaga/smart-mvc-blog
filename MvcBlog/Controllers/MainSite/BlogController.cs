using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcBlog.Models;
using MvcBlog.Domain;

namespace MvcBlog.Controllers.MainSite
{
    public class BlogController : Controller
    {
        private IBlogManager _blogManager;
        private IDBHelper _dbHelper;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="manager">IBlogManager implementation injected by Ninject</param>
        /// <param name="helper">IDBHelper implementation injected by Ninject</param>
        public BlogController(IBlogManager manager, IDBHelper helper)
        {
            _blogManager = manager;
            _dbHelper = helper;
        }

        //
        // GET: /Blog/

        public ActionResult Index(int id)
        {
            Domain.Entities.Blog blog = _blogManager.GetBlog(id, false, true);//.GetBlog(id);

            HomePageModel model = new HomePageModel
            {
                Categories = _dbHelper.GetCategories(),
                ActiveBlog = blog,
                Blogs = _blogManager.GetAllBlogs()
            };


            return View(model);
        }

    }
}
