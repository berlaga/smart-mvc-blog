﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcBlog.Models;
using MvcBlog.Domain;

namespace MvcBlog.Controllers.MainSite
{
    public class TagController : Controller
    {
        private IBlogManager _blogManager;
        private IDBHelper _dbHelper;


        public TagController(IBlogManager manager, IDBHelper helper)
        {
            _blogManager = manager;
            _dbHelper = helper;
        }

        
        //
        // GET: /Tag/

        public ActionResult Index(int id)
        {
            HomePageModel model = new HomePageModel
            {
                Categories = _dbHelper.GetCategories(),
                Blogs = _blogManager.GetBlogsByTagId(id)
            };

            ViewBag.TagName = _dbHelper.GetTag(id).Name;

            return View(model);
        }

    }
}
