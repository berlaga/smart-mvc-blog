﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcBlog.Models;
using MvcBlog.Domain;
using MvcBlog.Domain.Entities;



namespace MvcBlog.Controllers.Admin
{
    public class BlogAdminController : Controller
    {

        private IBlogManager _blogManager;
        private IDBHelper _dbHelper;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="manager"></param>
        public BlogAdminController(IBlogManager manager, IDBHelper helper)
        {
            //ninject: inject implementation
            _blogManager = manager;
            _dbHelper = helper;
        }



        //
        // GET: /BlogAdmin/Index
        [Authorize(Roles = "Admin")]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult Index()
        {
            string searchToken = string.Empty;
            if((HttpContext.Request.QueryString != null) && (HttpContext.Request.QueryString["searchToken"] != null))
            {
                searchToken = HttpContext.Request.QueryString["searchToken"].ToString();
            }

            var blogs = _blogManager.GetAllBlogs(string.IsNullOrEmpty(searchToken) ? null : searchToken);
            blogs.ForEach(PrepareBlogList);
            
            BlogsViewModel model = new BlogsViewModel
            {
                BlogList = blogs
            };


            return View(model);
        }

        //
        // GET: /BlogAdmin/BlogList/        
        public ActionResult BlogList()
        {
            
            BlogsViewModel model = new BlogsViewModel
            {
                BlogList = _blogManager.GetAllBlogs()
            };


            return View(model);
        }

        //
        // GET: /BlogAdmin/Tags/        
        [Authorize(Roles = "Admin")]
        public ActionResult Tags()
        {

            BlogsViewModel model = new BlogsViewModel
            {
                TagList = _dbHelper.GetTags()
            };


            return View(model);
        }

        //
        // GET: /BlogAdmin/TalkBacks/1  
        [Authorize(Roles = "Admin")]
        public ActionResult TalkBacks(int id)
        {
            BlogsViewModel model = new BlogsViewModel
            {
                NewBlog = _blogManager.GetBlog(id)//reuse NewBlog just to store blog ID
            };

            return View(model);
        }


        //
        // GET: /CreateBlog/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var selectItems = new SelectList(_dbHelper.GetCategories(), "Id", "Name");

            BlogsViewModel model = new BlogsViewModel
            {
                BlogList = _blogManager.GetAllBlogs(),
                CategoryList = selectItems
            };

            return View(model);
        }

        //
        // POST: /CreateBlog/Create

        [HttpPost]
        [ValidateInput(false)] /*this works in conjunction with web.config value requestValidationMode="2.0" http://msdn.microsoft.com/en-us/library/hh882339.aspx */
        [Authorize(Roles = "Admin")]
        //Custom binder if needed
        public ActionResult Create([ModelBinder(typeof(BlogModelBinder))]MvcBlog.Domain.Entities.Blog newBlog, FormCollection collection)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Please, try again!");

                var selectItems = new SelectList(_dbHelper.GetCategories(), "Id", "Name");

                BlogsViewModel model = new BlogsViewModel
                {
                    BlogList = _blogManager.GetAllBlogs(),
                    CategoryList = selectItems,
                    NewBlog = newBlog
                };


                return View(model);
            }

            try
            {
                newBlog.CreatedDate = DateTime.Now;
                newBlog.ModifiedDate = DateTime.Now;

                int res = _blogManager.CreateBlog(newBlog);
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw new HttpException(500, "Create failed.");
            }
        }

        //
        // GET: /CreateBlog/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var selectItems = new SelectList(_dbHelper.GetCategories(), "Id", "Name");

            MvcBlog.Domain.Entities.Blog blog = _blogManager.GetBlog(id);

            BlogsViewModel model = new BlogsViewModel
            {
                BlogList = _blogManager.GetAllBlogs(),
                CategoryList = selectItems,
                NewBlog = blog,
                CategoryId = blog.CategoryId
            };

            return View(model);
        }

        //
        // POST: /CreateBlog/Edit/5

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateInput(false)]
        public ActionResult Edit([ModelBinder(typeof(BlogModelBinder))]MvcBlog.Domain.Entities.Blog blog, FormCollection collection)
        //public ActionResult Edit(MvcBlog.Domain.Entities.Blog blog, FormCollection collection)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Please, try again!");

                var selectItems = new SelectList(_dbHelper.GetCategories(), "Id", "Name");

                BlogsViewModel model = new BlogsViewModel
                {
                    BlogList = _blogManager.GetAllBlogs(),
                    CategoryList = selectItems,
                    NewBlog = blog
                };


                return View(model);
            }


            try
            {
                blog.ModifiedDate = DateTime.Now;
                
                _blogManager.UpdateBlog(blog);
                
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw new HttpException(500, "Update failed.");
            }
        }


        //
        // POST: /CreateBlog/Delete/5

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _blogManager.DeleteBlog(id);

                return RedirectToAction("Index");

            }
            catch
            {
                throw new HttpException(500, "Delete failed.");
            }
        }

        /// <summary>
        /// Format Body and Description
        /// </summary>
        /// <param name="blog"></param>
        private static void PrepareBlogList(MvcBlog.Domain.Entities.Blog blog)
        {
            if (!string.IsNullOrEmpty(blog.Body))
            {
                if (blog.Body.Length > 30)
                {
                    blog.Body = blog.Body.Substring(0, 30) + "..";
                }
                else
                {
                    //do nothing
                }
            }

            if (!string.IsNullOrEmpty(blog.Description))
            {
                if (blog.Description.Length > 30)
                {
                    blog.Description = blog.Description.Substring(0, 30) + "..";
                }
                else
                {
                    //do nothing
                }
            }
        
        }
    }
}
