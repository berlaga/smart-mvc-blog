﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcBlog.Domain;
using Ninject;

namespace MvcBlog.Controllers.Admin
{
    /// <summary>
    /// Provides custom binding logic for a controller action
    /// </summary>
    public class BlogModelBinder : IModelBinder
    {
        //private IDBHelper _dbHelper;
        
        //[Inject]
        //public IDBHelper DbHelper
        //{
        //    get { return _dbHelper; }
        //    set { _dbHelper = value; }
        //}

        //TODO - Ninject injection

        private SimpleDBHelper _helper = new SimpleDBHelper();
        private SimpleBlogManager _manager = new SimpleBlogManager();

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            //http://www.dotnetcurry.com/ShowArticle.aspx?ID=584

            //bindingContext.ValueProvider.GetValue()

            if (controllerContext.RouteData.Values["action"].Equals("Create"))
            {
                var blog = new MvcBlog.Domain.Entities.Blog();
                blog.Body = controllerContext.HttpContext.Request.Form["Body"] ?? "";
                blog.Description = controllerContext.HttpContext.Request.Form["Description"] ?? "";
                blog.Title = controllerContext.HttpContext.Request.Form["Title"] ?? "";
                //blog.Category = _helper.GetCategory(int.Parse(controllerContext.HttpContext.Request.Form["BlogCategory"])),
                blog.CategoryId = !string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form["CategoryId"]) ?
                        int.Parse(controllerContext.HttpContext.Request.Form["CategoryId"]) : -1;// Do not initialize Category or new Category will be created

                //Tags
                if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form["Tags"]))
                {
                    string tags = controllerContext.HttpContext.Request.Form["Tags"] ?? "";
                    string[] arr = tags.Split(new char[] { ',' });

                    List<MvcBlog.Domain.Entities.Tag> tagList = new List<Domain.Entities.Tag>();
                    foreach (string s in arr)
                    {
                        MvcBlog.Domain.Entities.Tag t = new Domain.Entities.Tag();
                        t.Name = s.Trim();
                        tagList.Add(t);
                    }

                    if (tagList.Count > 0)
                        blog.Tags = tagList;
                }

                //return Blog object to controller
                return blog;
            }
            else //Edit
            {
                var blog = new MvcBlog.Domain.Entities.Blog();
                blog.Id = int.Parse(controllerContext.HttpContext.Request.Form["Id"]);
                blog.Body = controllerContext.HttpContext.Request.Form["Body"] ?? "";
                blog.Description = controllerContext.HttpContext.Request.Form["Description"] ?? "";
                blog.Title = controllerContext.HttpContext.Request.Form["Title"] ?? "";
                blog.CategoryId = !string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form["CategoryId"]) ?
                    int.Parse(controllerContext.HttpContext.Request.Form["CategoryId"]) : -1;// Do not initialize Category or new Category will be created

                blog.Tags = new List<Domain.Entities.Tag>();

                //Tags
                if (!string.IsNullOrEmpty(controllerContext.HttpContext.Request.Form["Tags"]))
                {
                    string tags = controllerContext.HttpContext.Request.Form["Tags"] ?? "";
                    string[] arr = tags.Split(new char[] { ',' });

                    foreach (string s in arr)
                    {
                        MvcBlog.Domain.Entities.Tag t = new Domain.Entities.Tag();
                        t.Name = s.Trim();
                        blog.Tags.Add(t);
                    }

                }

                //return Blog object to controller
                return blog;

            }

        }
    }
}