﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

using MvcBlog.Models;
using MvcBlog.Domain;
using MvcBlog.Domain.Entities;

namespace MvcBlog.Controllers.Admin
{
    public class BlogAdminHelperController : Controller
    {

        private IBlogManager _blogManager;
        private IDBHelper _dbHelper;


        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="manager"></param>
        public BlogAdminHelperController(IBlogManager manager, IDBHelper helper)
        {
            //ninject: inject implementation
            _blogManager = manager;
            _dbHelper = helper;

        }

        //
        // GET: /Index/
        public PartialViewResult Index()
        {
            BlogsViewModel model = new BlogsViewModel
            {
                BlogList = _blogManager.GetAllBlogs().OrderByDescending(p=>p.CreatedDate).ToList()
            };

            return PartialView(model);
        }

        //
        // GET: /TagList/

        public PartialViewResult TagList(List<MvcBlog.Domain.Entities.Tag> tagList)
        {
            return PartialView(tagList);
        }

        public PartialViewResult TagListCommaSeparated(List<MvcBlog.Domain.Entities.Tag> tagList)
        {
            StringBuilder sb = new StringBuilder();

            foreach (Tag t in tagList)
            {
                sb.Append(t.Name + ",");
            }

            string final = sb.ToString();

            if (final.Length > 0)
            {
                final = final.Remove(final.Length - 1, 1);
            }

            ViewBag.TagList = "<input type=\"text\" value = \""  + final + "\" id=\"Tags\" name=\"Tags\" />";

            return PartialView();
        }



        public PartialViewResult TagListFull()
        {
            List<MvcBlog.Domain.Entities.Tag> tagList = _dbHelper.GetTags().OrderBy(p => p.Name).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Append("<table style=\"width:200px;\" id=\"box-table-a\">\r\n");

            int count = tagList.Count / 3;

            if (tagList.Count % 3 != 0)
            {
                count++;
            }

            int k = 0;

            for (int i = 0; i < count; i++)
            {
                if (k > tagList.Count)
                    break;

                sb.Append("<tr>\r\n");

                for (int j = 0; j < 3; j++)
                {
                    if (k >= tagList.Count)
                    {
                        break;
                    }


                    sb.Append("<td>");

                    string url = Url.Action("Index", "Tag", new { id = string.Format("{0}", tagList[k].Id) });
                    sb.Append(string.Format("<a href=\"{0}\">", url) + tagList[k].Name + "</a>");
                    sb.Append("</td>\r\n");

                    k++;
                }                

                sb.Append("</tr>\r\n");

            }

            sb.Append("</table>");

            ViewBag.FormatedString = sb.ToString(); ;


            return PartialView();
        }

   
    }
}
