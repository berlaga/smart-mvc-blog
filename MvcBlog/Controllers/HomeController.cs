﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcBlog.Domain;
using MvcBlog.Models;

namespace MvcBlog.Controllers
{
    public class HomeController : Controller
    {
        private IBlogManager _blogManager;
        private IDBHelper _dbHelper;


        public HomeController(IBlogManager manager, IDBHelper helper)
        {
            _blogManager = manager;
            _dbHelper = helper;
        }

        public ActionResult Index()
        {
            HomePageModel model = new HomePageModel
            {
                Categories = _dbHelper.GetCategories(),
                Blogs = _blogManager.GetAllBlogs()
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Categories(int id)
        {
            ViewBag.CategoryName = _dbHelper.GetCategory(id).Name;

            HomePageModel model = new HomePageModel
            {
                Categories = _dbHelper.GetCategories(),
                Blogs = _blogManager.GetAllBlogs().Where(p=>p.CategoryId == id).ToList()
            };

            return View(model);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
