﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using MvcBlog.Domain;
using MvcBlog.Domain.Entities;

namespace MvcBlog.Controllers
{
    public class BlogController : ApiController
    {

        private IBlogManager _blogManager;


        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="manager"></param>
        public BlogController(IBlogManager manager)
        {
            //ninject: inject implementation
            _blogManager = manager;
        }

        // GET api/Blog
        public IEnumerable<Blog> GetBlogs()
        {
            return _blogManager.GetAllBlogs().AsEnumerable();
        }

        // GET api/Blog/5
        public Blog GetBlog(int id)
        {
            Blog blog = _blogManager.GetBlog(id);

            if (blog == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return blog;
        }

        // PUT api/Blog/5
        public HttpResponseMessage PutBlog(int id, Blog blog)
        {
            if (ModelState.IsValid && id == blog.Id)
            {
                try
                {
                    _blogManager.UpdateBlog(blog);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // POST api/Blog
        public HttpResponseMessage PostBlog(Blog blog)
        {
            if (ModelState.IsValid)
            {
                int id = _blogManager.CreateBlog(blog);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, blog);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = blog.Id }));
                return response;
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        // DELETE api/Blog/5
        public HttpResponseMessage DeleteBlog(int id)
        {
            Blog blog = _blogManager.GetBlog(id);
            if (blog == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                _blogManager.DeleteBlog(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, blog);
        }

    }
}