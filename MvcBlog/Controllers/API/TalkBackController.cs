﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using MvcBlog.Domain;
using MvcBlog.Domain.Entities;

using Newtonsoft.Json;

namespace MvcBlog.Controllers.API
{
    public class TalkBackController : ApiController
    {
        private ITalkbackManager _talkBackManager;

        public TalkBackController(ITalkbackManager manager)
        {
            _talkBackManager = manager;
        }

        //create TalkBack /api/TalkBack/
        [HttpPost]
        public HttpResponseMessage PostTalkBack(TalkbackReply reply)
        {
            if (ModelState.IsValid && reply != null)
            {
                try
                {
                    _talkBackManager.CreateTalkback(reply);
                }
                catch (Exception ex)
                {
                    //TODO return to AJAX call more spesific error
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }

        //get talkbacks for a blog api/TalkBack/id
        [HttpGet]
        public string GetTalkBacks(int id)
        {
            List <TalkbackReply> list = _talkBackManager.GetBlogTalkbacks(id);

            string s = JsonConvert.SerializeObject(list, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });

            return s;
        }


        //update talkback api/TalkBack/
        [HttpPut]
        public HttpResponseMessage UpdateTalkBack(int id, TalkbackReply updateTalkBack)
        {
            _talkBackManager.UpdateTalkback(updateTalkBack);

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        //update talkback api/TalkBack/
        [HttpDelete]
        public HttpResponseMessage DeleteTalkBack(int id)
        {
            try
            {
                _talkBackManager.DeleteTalkback(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

    }
}
