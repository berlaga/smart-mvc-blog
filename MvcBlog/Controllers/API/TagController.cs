﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using System.Net;

using MvcBlog.Domain;
using MvcBlog.Domain.Entities;


namespace MvcBlog.Controllers.API
{
    public class TagController : ApiController
    {
        private IDBHelper _dbHelper;

        public TagController(IDBHelper dbhelper)
        {
            _dbHelper = dbhelper;
        }

        //update Tag /api/Tag/id
        [HttpPut]
        public HttpResponseMessage PutTag(int id, Tag tag)
        {
            if (ModelState.IsValid && id == tag.Id)
            {
                try
                {
                    _dbHelper.UpdateTag(tag);
                }
                catch (Exception)
                {
                    //TODO return to AJAX call more spesific error
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }

        //DELETE Tag /api/Tag/id
        [HttpDelete]
        public HttpResponseMessage DeleteTag(int id)
        {
            try
            {
                _dbHelper.DeleteTag(id);
            }
            catch (Exception)
            {
                //TODO return to AJAX call more spesific error
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return Request.CreateResponse(HttpStatusCode.OK);

        }

        //create Tag /api/Tag/
        [HttpPost]
        public HttpResponseMessage PutTag(Tag tag)
        {
            if (ModelState.IsValid && tag != null)
            {
                try
                {
                    _dbHelper.CreateTag(tag);
                }
                catch (Exception)
                {
                    //TODO return to AJAX call more spesific error
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

        }



    }
}