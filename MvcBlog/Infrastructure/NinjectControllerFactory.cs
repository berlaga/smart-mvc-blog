﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;

using MvcBlog.Domain;


namespace MvcBlog.Infrastructure {
    
    /// <summary>
    /// Ninject controller factory
    /// </summary>
    public class NinjectControllerFactory : DefaultControllerFactory {
        private IKernel ninjectKernel;

        public NinjectControllerFactory() {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, 
            Type controllerType) {

            return controllerType == null 
                ? null 
                : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings() {
            // put additional bindings here
            ninjectKernel.Bind<IBlogManager>().To<SimpleBlogManager>();
            ninjectKernel.Bind<IDBHelper>().To<SimpleDBHelper>();
            ninjectKernel.Bind<IUserManager>().To<SimpleUserManager>();
            ninjectKernel.Bind<ITalkbackManager>().To<SimpleTalkbackManager>();

            ninjectKernel.Bind<ILoggingService>().To<MvcLoggingService>();

        }
    }
}