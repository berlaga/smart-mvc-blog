[assembly: WebActivator.PreApplicationStartMethod(typeof(MvcBlog.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(MvcBlog.App_Start.NinjectWebCommon), "Stop")]

namespace MvcBlog.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    /// <summary>
    /// Required for using WebAPI
    /// </summary>
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);

            // Install our Ninject-based IDependencyResolver into the Web API config
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new MvcBlog.Infrastructure.NinjectDependencyResolver(kernel);

            return kernel;

            //var kernel = new StandardKernel();
            //kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            //kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            //RegisterServices(kernel);
            //return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            // This is where we tell Ninject how to resolve service requests
            kernel.Bind<MvcBlog.Domain.IBlogManager>().To<MvcBlog.Domain.SimpleBlogManager>();
            kernel.Bind<MvcBlog.Domain.IDBHelper>().To<MvcBlog.Domain.SimpleDBHelper>();
            kernel.Bind<MvcBlog.Domain.IUserManager>().To<MvcBlog.Domain.SimpleUserManager>();
            kernel.Bind<MvcBlog.Domain.ITalkbackManager>().To<MvcBlog.Domain.SimpleTalkbackManager>();

        }        
    }
}
