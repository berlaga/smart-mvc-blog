﻿using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using MvcBlog.Infrastructure;

namespace MvcBlog
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            //hookup ninject (Not for WebAPI!)
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
            
            //remove XML formatter, so Json is returned from controllers
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            string configFile = Server.MapPath("~/log4net.xml.config");
            System.IO.FileInfo config = new System.IO.FileInfo(configFile);
            log4net.Config.XmlConfigurator.ConfigureAndWatch(config);

        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //required for Error controllers to work
            filters.Add(new HandleErrorAttribute());
        }
    }
}