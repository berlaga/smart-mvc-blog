﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Web.Mvc;
using WebMatrix.WebData;
using MvcBlog.Models;
using System.Web.Security;

namespace MvcBlog.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<MvcBlog.Domain.BlogsContext>(null);

                try
                {
                    using (var context = new MvcBlog.Domain.BlogsContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);


                    #region create roles if doesn't exist
                    
                    var roles = (SimpleRoleProvider)Roles.Provider;

                    if (!roles.RoleExists("Admin"))
                        roles.CreateRole("Admin");

                    if (!roles.RoleExists("User"))
                        roles.CreateRole("User");

                    #endregion



                    #region create default users

                    if (!WebSecurity.UserExists("admin"))
                    {
                        WebSecurity.CreateUserAndAccount("admin", "password", new { UserEmail = "admin@mvcblog.com" });
                        roles.AddUsersToRoles(new string[] { "admin" }, new string[] { "Admin" });
                    }

                    if (!WebSecurity.UserExists("site_user"))
                    {
                        WebSecurity.CreateUserAndAccount("site_user", "password", new { UserEmail = "site_user@mvcblog.com" });
                        roles.AddUsersToRoles(new string[] { "site_user" }, new string[] { "User" });
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }
}
