﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MvcBlog.Domain;

namespace MvcBlog.Models
{
    public class BlogsViewModel
    {
        public List<MvcBlog.Domain.Entities.Blog> BlogList { get; set; }
        public SelectList CategoryList { get; set; } //used for Categories drop down list
        public int CategoryId { get; set; }        
        public List<MvcBlog.Domain.Entities.Tag> TagList { get; set; }

        public MvcBlog.Domain.Entities.Blog NewBlog { get; set; } //used for POST actions only
    }
}