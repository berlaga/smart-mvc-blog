﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MvcBlog.Domain.Entities;

namespace MvcBlog.Models
{
    public class HomePageModel
    {
        public List<Category> Categories { get; set; }
        public List<Blog> Blogs { get; set; }

        public LoginModel LoginModel {get; set;}
        public RegisterModel RegisterModel { get; set; }

        public Blog ActiveBlog { get; set; }
    }
}